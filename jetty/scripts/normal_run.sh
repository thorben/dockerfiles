pre_start_action() {
  echo "Using existing PostgreSql database"
  cd $JETTY_BASE
}

post_start_action() {
  echo "Jetty is running"
}
