pre_start_action() {
  echo "Creating new Jetty Base"  
  mkdir -p $JETTY_BASE
  mkdir -p $JETTY_LOGS
  cd $JETTY_BASE
  java -jar $JETTY_HOME/start.jar --add-to-startd=server,http,deploy
  cp $JETTY_HOME/start.ini $JETTY_BASE/start.ini
  chown -R jetty:jetty $JETTY_BASE  
  chown -R jetty:jetty $JETTY_LOGS 
}

post_start_action() {
  echo "Jetty is running"
}
