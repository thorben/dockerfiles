#!/bin/bash
# Starts up postgresql within the container.

if [ ! -d $JETTY_BASE ]; then
  source /scripts/first_run.sh
else
  source /scripts/normal_run.sh
fi

wait_for_postgres_and_run_post_start_action() {
  # Wait for postgres to finish starting up first.
  while [[ ! -e $JETTY_PID ]] ; do
      inotifywait -q -e create /var/run/ >> /dev/null
  done

  post_start_action
}

pre_start_action

wait_for_postgres_and_run_post_start_action &

# Start Confluence
echo "Starting Jetty..."
setuser jetty $JETTY_HOME/bin/jetty.sh run