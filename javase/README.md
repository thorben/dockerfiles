# Java 7 Base Image

A base image with [oracle java 7](http://www.oracle.com/technetwork/java/javase) installed
It uses [Phusion's Baseimage](http://phusion.github.io/baseimage-docker/) as base image.


## Features ##

- [Oracle Java SE 7](http://www.oracle.com/technetwork/java/javase)
- UTF-8 Charset


## Exposed volumes ##

None.


## Exposed ports ##

None.


## Environment Variables

- `JAVA_HOME`: The location of the java home


## Usage ##

### Normal start with exposed network port

`docker run thstangenberg/javase:7 java -version`


## License ##
Published under the MIT License 
[LICENSE.md](https://bitbucket.org/thorben/dockerfiles/src/master/postgresql/LICENSE.md?at=master)