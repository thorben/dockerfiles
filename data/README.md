# A Docker Container for data files

A small data container for various data files. 
It uses [busybox](https://registry.hub.docker.com/_/busybox/) as base image and is just about 5.6 MB in size.


## Exposed volumes

1. `/data`: the directory to use for data storage


## Exposed ports

None

## Environment Variables

## Usage

Normal start
`docker run -d --name="data" thstangenberg/data`

Start with image volume `/data` mapped to `/tmp/some-data` of the host machine
`docker run -d --name="data" -v /tmp/some-data:/data thstangenberg/data`


## License
Published under the MIT License 
[LICENSE.md](https://bitbucket.org/thorben/dockerfiles/src/master/data/LICENSE.md?at=master)