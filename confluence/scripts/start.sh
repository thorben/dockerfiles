#!/bin/bash
# Starts up postgresql within the container.

POSTGRESQL_DATA=/data

if [ ! -e $POSTGRESQL_DATA/PG_VERSION ]; then
  source /scripts/first_run.sh
else
  source /scripts/normal_run.sh
fi

# wait_for_postgres_and_run_post_start_action() {
#   # Wait for postgres to finish starting up first.
#   while [[ ! -e /var/run/postgresql/9.3-main.pid ]] ; do
#       inotifywait -q -e create /run/postgresql/ >> /dev/null
#   done

#   post_start_action
# }

pre_start_action

# wait_for_postgres_and_run_post_start_action &

# Start Confluence
echo "Starting PostgreSQL..."
/opt/confluence/bin/start-confluence.sh -fg

echo "Waiting for post start..."