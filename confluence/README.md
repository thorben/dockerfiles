# Atlassian's Confluence Image

A docker image with [atlassian's confluence](https://www.atlassian.com/de/software/confluence).

## Base Image ##

[thstangenberg/javase:7](https://registry.hub.docker.com/u/thstangenberg/javase/) as baseimage.


## Features ##

- [atlassian's confluence 5.5](https://www.atlassian.com/de/software/confluence)
- UTF-8 Charset


## Exposed volumes ##

None.


## Exposed ports ##

- `8090`: Confluence default port.


## Environment Variables

- `JAVA_OPTS`: The java options for confluence. By default `JAVA_OPTS=-Xms256m -Xmx512m -XX:MaxPermSize=256m -Djava.awt.headless=true` is set.


## Usage ##

### Normal start with exposed network port

`docker run thstangenberg/confluence`


## License ##
Published under the MIT License 
[LICENSE.md](https://bitbucket.org/thorben/dockerfiles/src/master/postgresql/LICENSE.md?at=master)