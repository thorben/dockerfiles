# PostgreSQL 9.3 Docker Container

A docker container featuring [PostgreSQL 9.3](http://www.postgresql.org/). 
It uses [Phusion's Baseimage](http://phusion.github.io/baseimage-docker/) as base image.


## Features ##

- [PostgreSQL 9.3](http://www.postgresql.org/)
- UTF-8 Charset
- Multiple database support
- Easy backup/restore with a data container 


## PostgreSQL configuration ##

The configuration files postgresql.conf and pg_hba.conf are copied to the data directory on initialization.


## Exposed volumes ##

None. Use the data container `thstangenberg/postgresql:data`.


## Exposed ports ##

1. `5432` - The PostgreSQL default port.


## Environment Variables

- `DB`: The space seperated database name(s) to create on initialization. Default: `docker`
- `USER`: The username. Default: `docker`
- `PASS`: The password for `USER`. Default: `docker`


## Usage ## 

Normal start with exposed network port
`docker run -d -p 5432:5432 thstangenberg/postgresql:9.3`

Start with linked data volume from the data image
`docker run -d --name="pg-data" thstangenberg/postgresql:data`
`docker run -d --volumes-from "pg-data" thstangenberg/postgresql:9.3`

Start with linked data volume from the data image and provided variables
`docker run -d --name="pg-data" thstangenberg/postgresql:data`
`docker run -d -p 5432:5432 --volumes-from "pg-data" -e DB="myDB1 myDB2 myDB3" -e USER="super" -e PASS="confidential" thstangenberg/postgresql:9.3`


## License ##

see LICENSE.txt