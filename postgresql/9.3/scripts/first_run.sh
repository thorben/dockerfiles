USER=${USER:-docker}
PASS=${PASS:-docker}
DB=${DB:-docker}

pre_start_action() {
  # Echo out info to later obtain by running `docker logs container_name`
  echo "PostgreSQL Username = $USER"
  echo "PostgreSQL Password = $PASS"
  echo "PostgreSQL Database = $DB"

  mkdir -p $POSTGRESQL_DATA  
  chown -R postgres:postgres $POSTGRESQL_DATA
  sudo -u postgres /usr/lib/postgresql/9.3/bin/initdb -D $POSTGRESQL_DATA -E 'UTF8'
  ln -s /etc/ssl/certs/ssl-cert-snakeoil.pem $POSTGRESQL_DATA/server.crt
  ln -s /etc/ssl/private/ssl-cert-snakeoil.key $POSTGRESQL_DATA/server.key
  # Copy configuration files
  cp -f /etc/postgresql/9.3/main/postgresql.conf $POSTGRESQL_DATA
  cp -f /etc/postgresql/9.3/main/pg_hba.conf $POSTGRESQL_DATA

  # Ensure postgres owns the DATA_DIR
  chown -R postgres $POSTGRESQL_DATA
  # Ensure we have the right permissions set on the DATA_DIR
  chmod -R 700 $POSTGRESQL_DATA
}

post_start_action() {
  echo "Creating the user: $USER"
  setuser postgres psql -q <<-EOF
    CREATE ROLE $USER WITH ENCRYPTED PASSWORD '$PASS';
    ALTER USER $USER WITH ENCRYPTED PASSWORD '$PASS';
    ALTER ROLE $USER WITH SUPERUSER;
    ALTER ROLE $USER WITH LOGIN;
EOF

  # create database if requested
  if [ ! -z "$DB" ]; then
    for db in $DB; do
      echo "Creating database: $db"
      setuser postgres psql -q <<-EOF
      CREATE DATABASE $db WITH OWNER=$USER ENCODING='UTF8';
      GRANT ALL ON DATABASE $db TO $USER
EOF
    done
  fi

}
