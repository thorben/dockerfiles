# PostgreSQL Datafiles Docker Container

A small data container for the postgresql data files. 
It uses [busybox](https://registry.hub.docker.com/_/busybox/) as base image and is just about 5.6 MB in size.


## Exposed volumes ##

1. `/data`: the directory to use for data storage


## Exposed ports ##

None


## Usage ##

Normal start
`docker run -d --name="pg-data" thstangenberg/postgresql:data`

Start with image volume `/data` mapped to `/tmp/pg-data` of the host machine
`docker run -d --name="pg-data" -v /tmp/pg-data:/data thstangenberg/postgresql:data`


## License ##

see LICENSE.txt